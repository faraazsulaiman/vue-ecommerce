import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '../pages/HomePage';
import ContactUsPage from '../pages/ContactUsPage';
import ProductsPage from '../pages/ProductsPage';
import ProfilePage from '../pages/account/ProfilePage';
import RegisterPage from '../pages/account/RegisterPage';
import LoginPage from '../pages/account/LoginPage';

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomePage
		},
		{
			path: '/contact-us',
			name: 'contact-us',
			component: ContactUsPage
		},
		{
			path: '/products',
			name: 'products',
			component: ProductsPage
		},
		{
			path: '/account/profile',
			name: 'account-profile',
			component: ProfilePage
		},
		{
			path: '/account/orders',
			name: 'account-orders',
			component: ProfilePage
		},
		{
			path: '/account/register',
			name: 'account-register',
			component: RegisterPage
		},
		{
			path: '/account/login',
			name: 'account-login',
			component: LoginPage
		}
	],
});
