// initial state
const state = {
	user: {}
}

// getters
const getters = {
	getUser()
	{
		return state.user;
	}
}

// actions
const actions = {
	setUser(context, user)
	{
		context.commit('setUser', user);
	}
}

// mutations
const mutations = {
	setUser(state, user)
	{
		state.user = user;
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
